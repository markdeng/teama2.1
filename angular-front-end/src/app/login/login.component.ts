import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(public router: Router) {
      document.title = "Opal | Login";
    }

    ngOnInit() {}

    onLoggedin() {
      let u_email = (<HTMLInputElement>document.getElementById("user-email")).value;
      let u_password = (<HTMLInputElement>document.getElementById("user-password")).value;
      fetch(`http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=login&uid=${u_email}&pwd=${u_password}`, {
        method: "GET",
        credentials: 'include',
        //cache: 'no-cache',
        mode:'cors'
      })
      .then(response => response.json())
      .then(parsedResponse => {
        if(parsedResponse.status == "success"){
          console.log("Success");
          localStorage.setItem('isLoggedin', 'true');
          this.router.navigate(['/dashboard']);
          document.cookie = `name=${u_email}`;
        } else {
          console.log("Sorry, unauthorized access");
        }

      })
      //[routerLink]="['/dashboard']"
    }
}
