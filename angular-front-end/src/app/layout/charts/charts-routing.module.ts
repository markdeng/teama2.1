import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsComponent } from './charts.component';
import { CounterpartyComponent } from '../counterparty/counterparty.component';

const routes: Routes = [
    { path: '', component: ChartsComponent },
    { path: ':counterparty_name', loadChildren: '../counterparty/counterparty.module#CounterpartyModule' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChartsRoutingModule {}
