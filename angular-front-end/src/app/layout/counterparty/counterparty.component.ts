import { Component, OnInit } from '@angular/core';
// For MDB Angular Free
import { routerTransition } from '../../router.animations';
import { dashboard } from './myscripts'
import { StatModule } from '../../shared';

@Component({
    selector: 'app-counterparty',
    templateUrl: './counterparty.component.html',
    styleUrls: ['./counterparty.component.scss'],
    animations: [routerTransition()]
})
export class CounterpartyComponent implements OnInit {
  public currentCounterpartyName: string = "";
  public currentCounterpartyPrice: number = 0;
  public quanity_of_shares_currently_owned: number = 0;
  public totalDeals: number = 0;
  public currentCounterpartyDealHistory: Array<any> = [];
  public currentCounterpartyPriceHistory: Array<any> = [];
  public tenMostRecentTrades: Array<any> = [];
  public datapresent: boolean = false;
  public showingBuyData: boolean = true;
  public buyAndSellInstrumentData: object = {
    Galactia: { B: 0, S: 0 },
    Astronomica: { B: 0, S: 0 },
    Floral: { B: 0, S: 0 },
    Celestial: { B: 0, S: 0 },
    Heliosphere: { B: 0, S: 0 },
    Deuteronic: { B: 0, S: 0 },
    Jupiter: { B: 0, S: 0 },
    Koronis: { B: 0, S: 0 },
    Interstella: { B: 0, S: 0 },
    Lunatic: { B: 0, S: 0 },
    Borealis: { B: 0, S: 0},
    Eclipse: { B: 0, S: 0 },
 };
 public instrumentNames= ["Galactia", "Astronomica", "Floral", "Celestial", "Heliosphere", "Deuteronic",
                          "Jupiter",  "Koronis",  "Interstella", "Lunatic", "Borealis", "Eclipse" ]

  // lineChart
  public lineChartData: Array<any> = [
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
      { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
      { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C' }
  ];

  public instrumentColors = ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#c2c645",
                     "#67cc59", "#5b64e5", "#a371ce", "#e7ef70", "#c47bad","#ea995d"];

  public lineChartLabels: Array<any> = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July'
  ];
  public lineChartOptions: any = { responsive: true };

  public lineChartColors: Array<any> = [
      {
          // grey
          backgroundColor: 'rgba(148,159,177,0.2)',
          borderColor: 'rgba(148,159,177,1)',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      {
          // dark grey
          backgroundColor: 'rgba(77,83,96,0.2)',
          borderColor: 'rgba(77,83,96,1)',
          pointBackgroundColor: 'rgba(77,83,96,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(77,83,96,1)'
      },
      {
          // grey
          backgroundColor: 'rgba(148,159,177,0.2)',
          borderColor: 'rgba(148,159,177,1)',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';

  // Doughnut
  public doughnutChartLabels: string[] = [
      "Astronomica", "Deuteronic", "Floral", "Galactia", "Celestial", "Heliosphere", "Jupiter", "Interstella", "Koronis", "Eclipse", "Borealis", "Lunatic"
  ];
  public doughnutChartData: number[] = [350, 450, 100, 123, 123, 425, 245, 765, 754, 222, 432, 234];
  public doughnutChartType: string = 'doughnut';



    constructor() {
      let counterpartyDomainName = window.location.href;
      let counterpartyName = counterpartyDomainName.substr(counterpartyDomainName.indexOf("counterparties/") + 15);
      this.currentCounterpartyName = counterpartyName;
      this.currentCounterpartyName = this.currentCounterpartyName.charAt(0).toUpperCase() + this.currentCounterpartyName.substr(1);
      fetch(`http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=deals&filter=Counterparty;Equals;${this.currentCounterpartyName}`, {
        method: 'GET',
        credentials: 'include',
        mode:'cors'
      })
      .then(response => response.json())
      .then(myJson => {
        myJson.deals.forEach(inDeal => {
          this.buyAndSellInstrumentData[inDeal.instrument_name][inDeal.deal_type] += parseInt(inDeal.deal_quantity);
        })
        //get deals array
        this.currentCounterpartyDealHistory = myJson.deals;
        //get count of deals
        this.totalDeals = this.currentCounterpartyDealHistory.length;
        //fet most recent deal info
        let mostRecentDeal= this.currentCounterpartyDealHistory[this.totalDeals - 1];
        //get current price of counterparty
        this.currentCounterpartyPrice = parseFloat((mostRecentDeal.deal_amount / mostRecentDeal.deal_quantity).toFixed(2));
        //get sum of shares bought
        let quanity_of_shares_owned: number = 0;;
        //get current count of shares currently owned
        this.currentCounterpartyDealHistory.forEach(unit => { quanity_of_shares_owned = (unit.deal_type == "B")?  (quanity_of_shares_owned + parseInt(unit.deal_quantity, 10)) : (quanity_of_shares_owned - parseInt(unit.deal_quantity, 10)) })
        this.quanity_of_shares_currently_owned = quanity_of_shares_owned;
        //get array of shareprice timeline
        this.currentCounterpartyDealHistory.forEach(unit => { this.currentCounterpartyPriceHistory.push({price: (unit.deal_amount / unit.deal_quantity), time: unit.time }) })
        //get ten most recent Trades
        this.tenMostRecentTrades = this.currentCounterpartyDealHistory.slice(this.totalDeals - 10, this.totalDeals);
        this.datapresent = true;

        let elementIdBuy = "doughnutChart";
        let buyData: Array<any> = [];
        let sellData: Array<any> = [];
        let labels: Array<any> =["Astronomica", "Deuteronic", "Floral", "Galactia", "Celestial", "Heliosphere", "Jupiter", "Interstella", "Koronis", "Eclipse", "Borealis", "Lunatic"];
        labels.forEach(label => {
          buyData.push(this.buyAndSellInstrumentData[label].B);
          sellData.push(this.buyAndSellInstrumentData[label].S)
        })
        this.paintPieChart(elementIdBuy, buyData, labels);
      })
    }

    ngOnInit() {
      dashboard('#dashboardabc');
    }

    public paintPieChart = (elementID, dataSet, iLabels) => {
      this.doughnutChartData = dataSet;
      this.doughnutChartLabels = iLabels;
      this.doughnutChartLabels = iLabels;
    }

    public switchBuySellData = () => {
      let elementIdBuy = "doughnutChart";
      let buyData: Array<any> = [];
      let sellData: Array<any> = [];
      let labels: Array<any> =["Astronomica", "Deuteronic", "Floral", "Galactia", "Celestial", "Heliosphere", "Jupiter", "Interstella", "Koronis", "Eclipse", "Borealis", "Lunatic"];
      labels.forEach(label => {
        buyData.push(this.buyAndSellInstrumentData[label].B);
        sellData.push(this.buyAndSellInstrumentData[label].S)
      })

      if(this.showingBuyData){
        this.paintPieChart(elementIdBuy, sellData, labels);
        this.showingBuyData = false;
      }else{
        this.paintPieChart(elementIdBuy, buyData, labels);
        this.showingBuyData = true;
      }

    }
}
