import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { CounterpartyComponent } from './counterparty.component'
import { counterpartyModule } from './counterparty.module'

describe('CounterpartyComponent', () => {
  let component: CounterpartyComponent
  let fixture: ComponentFixture<CounterpartyComponent>

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          CounterpartyModule,
          BrowserAnimationsModule,
          RouterTestingModule,
         ],
      }).compileComponents()
    })
  )

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterpartyComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
