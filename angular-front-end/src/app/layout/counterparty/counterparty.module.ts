import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { CounterpartyRoutingModule } from './counterparty-routing.module';
import { CounterpartyComponent } from './counterparty.component';
import { PageHeaderModule } from './../../shared';
import { StatModule } from '../../shared';

@NgModule({
    imports: [CommonModule, Ng2Charts, CounterpartyRoutingModule, PageHeaderModule, StatModule],
    declarations: [CounterpartyComponent]
})
export class CounterpartyModule {}
