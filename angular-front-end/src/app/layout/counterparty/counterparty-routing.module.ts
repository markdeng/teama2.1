import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CounterpartyComponent } from './counterparty.component';

const routes: Routes = [
    {
        path: '', component: CounterpartyComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CounterpartyRoutingModule {
}
