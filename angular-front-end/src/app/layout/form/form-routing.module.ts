import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form.component';
import { InstrumentComponent } from '../instrument/instrument.component';

const routes: Routes = [
        {path: '', component: FormComponent},
        { path: ':instrument_name', loadChildren: '../instrument/instrument.module#InstrumentModule' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FormRoutingModule {
}
