import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { PageHeaderModule } from './../../shared';
import { StatModule } from '../../shared';
import {Router} from '@angular/router';

@NgModule({
    imports: [CommonModule, FormRoutingModule, PageHeaderModule, StatModule],
    declarations: [FormComponent]
})
export class FormModule {}
