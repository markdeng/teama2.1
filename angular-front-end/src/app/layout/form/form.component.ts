import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    animations: [routerTransition()]
})
export class FormComponent implements OnInit {
    constructor(public router: Router) {
      document.title = "Opal | Instruments";
    }

    ngOnInit() {}

    public findInstrument(){
      console.log("HIT")
      let instrumentName=(<HTMLInputElement>document.getElementById("usr"))
      console.log(instrumentName.value);
      if (instrumentName.toString() != ""){
        console.log("$###")
        console.log([`instrument/${instrumentName.value}`])
        this.router.navigate([`instruments/${(instrumentName.value).toString().toLowerCase()}`])
      }
    }
}
