import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss'],
    animations: [routerTransition()]
})
export class TablesComponent implements OnInit {
  public rawdata: Array<any> = [];
  public transactions: boolean = false;
  public tranTracker: number = 20000;
  public resultsPerPage: number = 400;

  constructor() {
      document.title = "Opal | Trading Data";
      fetch('http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=login&uid=selvyn&pwd=gradprog2016',
      {
        method: "GET",
        credentials: 'include',
        mode:'cors'
      })
      .then(response => response.json())
      .then( myresponse => {
        fetch(`http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=deals&filter=ID;LessEquals;${this.tranTracker + this.resultsPerPage}&filter=ID;GreaterEquals;${this.tranTracker}`, {
          method: 'GET', credentials: 'include', mode:'cors'
        })
        .then(response => response.json())
        .then(myJson => {
          this.rawdata = myJson.deals;
          this.transactions = true;
          this.tranTracker += this.resultsPerPage;
        })
      })
  }

    ngOnInit() {}

    public nextResults = ()=>{
      this.getNewResults(true);

    }

    public prevResults = ()=>{
      this.getNewResults(false);
    }

    public getNewResults = (nextOnes)=>{
      if(nextOnes){
        console.log("Hit Next")
      }else{
        console.log("Hit Prev")
        this.tranTracker -= this.resultsPerPage;
      }
      fetch('http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=login&uid=selvyn&pwd=gradprog2016',
      {
        method: "GET", credentials: 'include', mode:'cors'
      })
      .then(response => response.json())
      .then( myresponse => {
        fetch(`http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=deals&filter=ID;LessEquals;${this.tranTracker + this.resultsPerPage}&filter=ID;GreaterEquals;${this.tranTracker}`, {
          method: 'GET', credentials: 'include', mode:'cors'
        })
        .then(response => response.json())
        .then(myJson => {
          console.log(this.tranTracker)
          this.rawdata = myJson.deals;
          this.transactions = true;
          if(nextOnes){
            this.tranTracker += this.resultsPerPage;
          }
        })
      })

    }
}
