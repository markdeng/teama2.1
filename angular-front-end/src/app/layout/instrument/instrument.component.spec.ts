import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { InstrumentComponent } from './instrument.component'
import { instrumentModule } from './instrument.module'

describe('InstrumentComponent', () => {
  let component: InstrumentComponent
  let fixture: ComponentFixture<InstrumentComponent>

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          InstrumentModule,
          BrowserAnimationsModule,
          RouterTestingModule,
         ],
      }).compileComponents()
    })
  )

  beforeEach(() => {
    fixture = TestBed.createComponent(InstrumentComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
