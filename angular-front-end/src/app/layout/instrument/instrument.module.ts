import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { InstrumentRoutingModule } from './instrument-routing.module';
import { InstrumentComponent } from './instrument.component';
import { PageHeaderModule } from './../../shared';
import { StatModule } from '../../shared';

@NgModule({
    imports: [CommonModule, Ng2Charts, InstrumentRoutingModule, PageHeaderModule, StatModule],
    declarations: [InstrumentComponent]
})
export class InstrumentModule {}
