import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { dashboard } from './myscripts'
import { StatModule } from '../../shared';

@Component({
    selector: 'app-instrument',
    templateUrl: './instrument.component.html',
    styleUrls: ['./instrument.component.scss'],
    animations: [routerTransition()]
})
export class InstrumentComponent implements OnInit {
  public currentInstrumentName: string = "";
  public currentInstrumentPrice: number = 0;
  public quanity_of_shares_currently_owned: number = 0;
  public totalDeals: number = 0;
  public currentInstrumentDealHistory: Array<any> = [];
  public currentInstrumentPriceHistory: Array<any> = [];
  public tenMostRecentTrades: Array<any> = [];
  public datapresent: boolean = false;
  public averageSellPrice: number = 0;
  public averageBuyPrice: number = 0;
  public instrumentVolatility: number = 0;

  public InstrumentCounterPartyData: Array<any>=[
    {State:'Selvyn',freq:{Buying:0, Selling:0}},
    {State:'Lina',freq:{Buying:0, Selling:0}},
    {State:'Richard',freq:{Buying:0, Selling:0}},
    {State:'Lewis',freq:{Buying:0, Selling:0}},
    {State:'John',freq:{Buying:0, Selling:0}},
    {State:'Nidia',freq:{Buying:0, Selling:0}}];

  // lineChart
  public lineChartData: Array<any> = [
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },

  ];
  public lineChartLabels: Array<any> = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July'
  ];
  public lineChartOptions: any = {
      responsive: true
  };
  public lineChartColors: Array<any> = [
      {
          // grey
          backgroundColor: 'rgba(148,159,177,0.2)',
          borderColor: 'rgba(148,159,177,1)',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      {
          // dark grey
          backgroundColor: 'rgba(77,83,96,0.2)',
          borderColor: 'rgba(77,83,96,1)',
          pointBackgroundColor: 'rgba(77,83,96,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(77,83,96,1)'
      },
      {
          // grey
          backgroundColor: 'rgba(148,159,177,0.2)',
          borderColor: 'rgba(148,159,177,1)',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';


    constructor() {
      this.instrumentVolatility = parseFloat(Math.random().toFixed(2));
      let instrumentDomainName = window.location.href;
      let instrumentName = instrumentDomainName.substr(instrumentDomainName.indexOf("instruments/") + 12);
      this.currentInstrumentName = instrumentName;
      this.currentInstrumentName = this.currentInstrumentName.charAt(0).toUpperCase() + this.currentInstrumentName.substr(1);
      console.log(this.currentInstrumentName);
      fetch(`http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=deals&filter=Instrument;Equals;${this.currentInstrumentName}`, {
        method: 'GET',
        credentials: 'include',
        //cache: 'no-cache',
        mode:'cors'
      })
      .then(response => response.json())
      .then(myJson => {
        //get deals array
        this.currentInstrumentDealHistory = myJson.deals;

        //get count of deals
        this.totalDeals = this.currentInstrumentDealHistory.length

        //fet most recent deal info
        let mostRecentDeal= this.currentInstrumentDealHistory[this.totalDeals - 1];

        //get current price of instrument
        this.currentInstrumentPrice = parseFloat((mostRecentDeal.deal_amount));

        //get sum of shares bought
        let quanity_of_shares_owned: number = 0;;

        //get current count of shares currently owned
        this.currentInstrumentDealHistory.forEach(unit => {
          if(unit.deal_type == "B"){
            quanity_of_shares_owned += parseInt(unit.deal_quantity, 10);
          }else{
            quanity_of_shares_owned -= parseInt(unit.deal_quantity, 10);
          }
        })
        this.quanity_of_shares_currently_owned = quanity_of_shares_owned;

        //get array of shareprice timeline
        this.currentInstrumentDealHistory.forEach(unit => {
          this.currentInstrumentPriceHistory.push({price: (unit.deal_amount), time: unit.time })
        })

        //get ten most recent Trades
        this.tenMostRecentTrades = this.currentInstrumentDealHistory.slice(this.totalDeals - 10, this.totalDeals);
        this.datapresent = true;


        let counterpartyMap = { Selvyn: 0, Lina: 1, Richard: 2, Lewis: 3, John: 4, Nidia: 5 };
        this.currentInstrumentDealHistory.forEach(transaction => {
          if(transaction.deal_type == "S"){
            this.InstrumentCounterPartyData[counterpartyMap[transaction.counterparty_name]].freq.Selling += parseInt(transaction.deal_quantity);
          }else{
            this.InstrumentCounterPartyData[counterpartyMap[transaction.counterparty_name]].freq.Buying += parseInt(transaction.deal_quantity);
          }
        })

        dashboard('#dashboardabc', this.InstrumentCounterPartyData);

        let dates: Array<any> = [];
        let prices: Array<any> = [];
        this.currentInstrumentPriceHistory.forEach(myprice =>{
          prices.push(myprice.price);
          dates.push(myprice.time);
        })
        this.lineChartData[0].data = prices;
        this.lineChartLabels = dates;
        this.getAvgPrices();
      })
    }

    ngOnInit() { }

    public getAvgPrices(){
      //;${this.currentInstrumentName}`
      console.log(this.currentInstrumentName);
      fetch(`http://10.11.32.20:81/dbanalyzer-a2-1/api.jsp?function=special&type=averages`, {
        method: 'GET',
        credentials: 'include',
        //cache: 'no-cache',
        mode:'cors'
      })
      .then(response => response.json())
      .then(myJson => {

        console.log(myJson.result);
        let instrumentBuyIndex = 0;
        let instrumentSellIndex = 0;
        myJson.result.forEach((unit, iter) => {
          if(unit.deal_type == "B" && unit.instrument_name.toLowerCase() == this.currentInstrumentName.toLowerCase()){
            console.log("HIT Buy")
            console.log(unit['AVG(deal_amount)'])
            this.averageBuyPrice = unit['AVG(deal_amount)']
          }
          if(unit.deal_type == "S" && unit.instrument_name.toLowerCase() == this.currentInstrumentName.toLowerCase()){
            console.log("HIT Sell")
            this.averageSellPrice = unit['AVG(deal_amount)']
          }
        })
      })
    }
}
