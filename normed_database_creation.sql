DROP DATABASE IF EXISTS db_grad_cs_1917;
CREATE DATABASE db_grad_cs_1917;
USE db_grad_cs_1917;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` char(40) NOT NULL,
  `user_pwd` char(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO users (user_id, user_pwd) SELECT user_id, user_pwd FROM db_grad.users;

DROP TABLE IF EXISTS `instrument`;
CREATE TABLE `instrument` (
  `instrument_id` INT NOT NULL AUTO_INCREMENT,
  `instrument_name` char(40) NOT NULL,
  PRIMARY KEY (`instrument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO instrument (instrument_name)
SELECT DISTINCT instrument_name FROM db_grad.deal;

DROP TABLE IF EXISTS `counterparty`;
CREATE TABLE `counterparty` (
  `counterparty_id` INT NOT NULL AUTO_INCREMENT,
  `counterparty_name` char(40) NOT NULL,
  `counterparty_status` char(40) NOT NULL,
  `counterparty_date_registered` DATETIME NOT NULL,
  PRIMARY KEY (`counterparty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO counterparty (counterparty_name, counterparty_status, counterparty_date_registered)
SELECT DISTINCT counterparty_name, counterparty_status, counterparty_date_registered FROM db_grad.deal;

DROP TABLE IF EXISTS `deal`;
CREATE TABLE `deal` (
  `deal_id` INT,
  `deal_time` DATETIME,
  `deal_type` char(40),
  `deal_amount` DECIMAL(12,2),
  `deal_quantity` INT,
  `deal_counterparty_id` INT,
  `deal_instrument_id` INT,
  PRIMARY KEY (`deal_id`),
  FOREIGN KEY (`deal_counterparty_id`)
    REFERENCES `counterparty`(`counterparty_id`)
    ON DELETE CASCADE,
  FOREIGN KEY (`deal_instrument_id`)
    REFERENCES `instrument`(`instrument_id`)
    ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO deal (deal_id,
	deal_time,
  deal_type,
  deal_amount,
  deal_quantity,
  deal_counterparty_id,
  deal_instrument_id)
SELECT deal_id, deal_time, deal_type, deal_amount, deal_quantity, counterparty.counterparty_id, instrument.instrument_id
FROM db_grad.deal
LEFT JOIN counterparty
ON deal.counterparty_name = counterparty.counterparty_name
LEFT JOIN instrument
ON deal.instrument_name = instrument.instrument_name;