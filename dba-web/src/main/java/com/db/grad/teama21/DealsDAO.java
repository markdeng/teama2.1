package com.db.grad.teama21;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.db.grad.teama21.DBFilter.Columns;
import com.db.grad.teama21.DBFilter.Comparators;

public class DealsDAO {
	public static ResultSet getDeals() {
	    DBConnector db = new DBConnector();
	    return db.getDeals();
	}
	
	public static ResultSet getDeals(String[] filters) {
		return getDeals(filters, false);
	}
	
	public static ResultSet getDeals(String[] filters, boolean orderDesc) {
	    DBConnector db = new DBConnector();
	    ArrayList<DBFilter> dbFilters = new ArrayList<>();
	    for (String f: filters) {
	    	dbFilters.add(parseFilter(f));
	    }
	    return db.getDeals(dbFilters, orderDesc);

	}
	
	private static DBFilter parseFilter(String filter) {
		String[] split = filter.split(";");
		DBFilter.Columns columnFilter = Columns.valueOf(split[0].trim());
		if (columnFilter == null) {
			return null;
		}
		
		DBFilter.Comparators comparator = Comparators.valueOf(split[1].trim());
		
		return new DBFilter(columnFilter, comparator, split[2].trim());
	}
}
