package com.db.grad.teama21;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class SpecialBean {
private ResultSet data;
	
	public void setData(ResultSet data) {
		this.data = data;
	}
	
	public boolean canProceed() {
		try {
			return !(data.isLast() || data.isAfterLast());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return false;
	}
	
	public String getRow() {
		try {
			data.next();
			int colcount = data.getMetaData().getColumnCount();
			String result = new String();
			for (int i = 1; i <= colcount; i++) {
				result += data.getString(i) + (i==colcount ? "" : ", ");
			}
			return result;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
	
	public String getRowJSON() {
		try {
			data.next();
			ResultSetMetaData rsmd = data.getMetaData();
			int colcount = rsmd.getColumnCount();
			String result = "{";
			for (int i = 1; i <= colcount; i++) {
				result += "\"" + rsmd.getColumnName(i) + "\":\"" + data.getString(i) + "\"" + (i==colcount ? "}" : ", ");
			}
			return result;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
}
