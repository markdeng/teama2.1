package com.db.grad.teama21;

import java.sql.ResultSet;

public class SpecialDAO {
	public static ResultSet getAverages() {
		DBConnector db = new DBConnector();
		return db.getAverages();
	}

	public static ResultSet getFinalPositions() {
		DBConnector db = new DBConnector();
		return db.getFinalPositions();
	}
	
	public static ResultSet getRealised() {
		DBConnector db = new DBConnector();
		return db.getRealised();
	}
	
}
