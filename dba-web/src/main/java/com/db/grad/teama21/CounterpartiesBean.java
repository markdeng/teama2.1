package com.db.grad.teama21;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CounterpartiesBean {
private ResultSet data;
	
	public void setData(ResultSet data) {
		this.data = data;
	}
	
	public boolean canProceed() {
		try {
			return !(data.isLast() || data.isAfterLast());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return false;
	}
	
	public String getRow() {
		try {
			data.next();
			int counterparty_id = data.getInt(1);
			String counterparty_name = data.getString(2);
			String counterparty_status = data.getString(3);
			Date counterparty_date_registered = data.getDate(4);
			return new String(counterparty_id + ", " + counterparty_name + ", " + 
					counterparty_status + ", " + counterparty_date_registered + "<br>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
	
	public String getRowJSON() {
		try {
			data.next();
			int counterparty_id = data.getInt(1);
			String counterparty_name = data.getString(2);
			String counterparty_status = data.getString(3);
			Date counterparty_date_registered = data.getDate(4);
			return new String("{\"counterparty_id\":\"" + counterparty_id + "\",\"counterparty_name\":\"" + counterparty_name + 
					"\", \"counterparty_status\":\"" + counterparty_status + "\",\"counterparty_date_registered\":\"" + counterparty_date_registered + "\"}");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
}
