package com.db.grad.teama21;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DealsBean {
	private ResultSet data;
	
	public void setData(ResultSet data) {
		this.data = data;
	}
	
	public boolean canProceed() {
		try {
			return !(data.isLast() || data.isAfterLast());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return false;
	}
	
	public String getRow() {
		try {
			data.next();
			int deal_id = data.getInt(1);
			String time = data.getString(2);
			String deal_type = data.getString(3);
			double deal_amount = data.getDouble(4);
			int deal_quantity = data.getInt(5);
			String instrument_name = data.getString(6);
			String counterparty_name = data.getString(7);
			String counterparty_status = data.getString(8);
			Date counterparty_date_registered = data.getDate(9);
			return new String(deal_id + ", " + time + ", " + deal_type + ", " + deal_amount + ", " + 
					deal_quantity + ", " + instrument_name + ", " + counterparty_name + ", " + 
					counterparty_status + ", " + counterparty_date_registered + "<br>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
	
	public String getRowJSON() {
		try {
			data.next();
			int deal_id = data.getInt(1);
			String time = data.getString(2);
			String deal_type = data.getString(3);
			double deal_amount = data.getDouble(4);
			int deal_quantity = data.getInt(5);
			String instrument_name = data.getString(6);
			String counterparty_name = data.getString(7);
			String counterparty_status = data.getString(8);
			Date counterparty_date_registered = data.getDate(9);
			return new String("{\"deal_id\":\"" + deal_id + "\", \"time\":\"" + time + "\", \"deal_type\":\"" + deal_type + "\",\"deal_amount\":\"" + deal_amount + 
					"\", \"deal_quantity\":\"" + deal_quantity + "\", \"instrument_name\":\"" + instrument_name + "\",\"counterparty_name\":\"" + counterparty_name + 
					"\", \"counterparty_status\":\"" + counterparty_status + "\",\"counterparty_date_registered\":\"" + counterparty_date_registered + "\"}");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
}
