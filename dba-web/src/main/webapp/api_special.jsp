<%@page contentType="application/json" pageEncoding="UTF-8"
	import="com.db.grad.teama21.SpecialDAO"%>
<jsp:useBean id="special" class="com.db.grad.teama21.SpecialBean"
	scope="application" />

<%
	//Class.forName("com.mysql.jdbc.Driver");
	if (session.getAttribute("session") == "TRUE") {
		String fun = request.getParameter("type");
		if (fun == null) fun = "";
		boolean valid = false;
		switch (fun) {
		case "averages": 
			special.setData(SpecialDAO.getAverages());
			valid = true;
			break;
		case "final":
			special.setData(SpecialDAO.getFinalPositions());
			valid = true;
			break;
		case "realised":
			special.setData(SpecialDAO.getRealised());
			valid = true;
			break;
		}
		if (valid){
			boolean first = true;
			out.print("{\"status\":\"success\",\"result\":[");
			while (special.canProceed()) {
				out.println((first ? "" : ",") + special.getRowJSON());
				first = false;
			}
			out.println("]}");
		} else {
			out.print("{\"status\":\"invalid\"}");
		}


	} else {
		out.print("{\"status\":\"unauthorised\"}");
	}
%>
