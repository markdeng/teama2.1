<%@page contentType="application/json" pageEncoding="UTF-8"
	import="com.db.grad.teama21.DealsDAO"%>
<jsp:useBean id="deals" class="com.db.grad.teama21.DealsBean"
	scope="application" />

<%
	//Class.forName("com.mysql.jdbc.Driver");
	if (session.getAttribute("session") == "TRUE") {
		String[] filters = request.getParameterValues("filter");
		String order = request.getParameter("order");
		boolean orderDesc = false;
		if (order != null && order.toUpperCase().equals("DESC")) {
			orderDesc = true;
		}
		out.print("{\"status\":\"success\",\"deals\":[");
		if (filters == null) {
			filters = new String[0];
		}
		deals.setData(DealsDAO.getDeals(filters, orderDesc));
		boolean first = true;
		while (deals.canProceed()) {
			out.println((first ? "" : ",") + deals.getRowJSON());
			first = false;
		}
		out.println("]}");

	} else {
		out.print("{\"status\":\"unauthorised\"}");
	}
%>
