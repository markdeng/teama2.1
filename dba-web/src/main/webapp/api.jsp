<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
   // Returns as json.
   response.setContentType("application/json");
   response.setHeader("Content-Disposition", "inline");
   response.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
   response.setHeader("Access-Control-Allow-Credentials", "true");

%>
<%  
	String[] functions = {"login", "deals","users","counterparties","instruments", "special"};
	String function = request.getParameter("function");
	boolean found = false;
	String f = "api_null.jsp";
	for (String fun: functions){
		if (fun.equals(function)){
			found = true;
			f = "api_" + function + ".jsp";
		}
	}
%>
<jsp:include page="<%=f %>"/>