<%@page contentType="application/json" pageEncoding="UTF-8"
	import="com.db.grad.teama21.UserDAO"%>
<jsp:useBean id="user" class="com.db.grad.teama21.UserBean"
	scope="application" />

<%
	Class.forName("com.mysql.jdbc.Driver");

	user.setUid(request.getParameter("uid"));
	user.setPwd(request.getParameter("pwd"));
	user = UserDAO.login(user);
	if (user.isValid()) {
		session.setAttribute("session", "TRUE");
		session.setAttribute("uid", user.getUid());

		out.println("{\"status\":\"success\",\"uid\":\"" + user.getUid() + "\"}");
	} else {
		out.println("{\"status\":\"unauthorised\"}");
	}
%>