<%@page contentType="application/json" pageEncoding="UTF-8"
	import="com.db.grad.teama21.CounterpartiesDAO"%>
<jsp:useBean id="counterparties" class="com.db.grad.teama21.CounterpartiesBean"
	scope="application" />

<%
	//Class.forName("com.mysql.jdbc.Driver");
	if (session.getAttribute("session") == "TRUE") {
		counterparties.setData(CounterpartiesDAO.getCounterparties());
		boolean first = true;
		out.print("{\"status\":\"success\",\"counterparties\":[");
		while (counterparties.canProceed()) {
			out.println((first ? "" : ",") + counterparties.getRowJSON());
			first = false;
		}
		out.println("]}");

	} else {
		out.print("{\"status\":\"unauthorised\"}");
	}
%>
