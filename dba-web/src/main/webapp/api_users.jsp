<%@page contentType="application/json" pageEncoding="UTF-8"
	import="com.db.grad.teama21.UserStatsDAO"%>
<jsp:useBean id="stats" class="com.db.grad.teama21.UserStatsBean"
	scope="application" />

<%
	//Class.forName("com.mysql.jdbc.Driver");
	if (session.getAttribute("session") == "TRUE") {
		out.print("{\"status\":\"success\",\"users\":[");

		stats.setData(UserStatsDAO.getUsers());
		boolean first = true;
		while (stats.canProceed()) {
			out.println((first ? "" : ",") + stats.getRowJSON());
			first = false;
		}
		out.println("]}");

	} else {
		out.print("{\"status\":\"unauthorised\"}");
	}
%>
