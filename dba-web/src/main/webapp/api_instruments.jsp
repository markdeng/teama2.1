<%@page contentType="application/json" pageEncoding="UTF-8"
	import="com.db.grad.teama21.InstrumentsDAO"%>
<jsp:useBean id="instruments" class="com.db.grad.teama21.InstrumentsBean"
	scope="application" />

<%
	//Class.forName("com.mysql.jdbc.Driver");
	if (session.getAttribute("session") == "TRUE") {
		instruments.setData(InstrumentsDAO.getInstruments());
		boolean first = true;
		out.print("{\"status\":\"success\",\"instruments\":[");
		while (instruments.canProceed()) {
			out.println((first ? "" : ",") + instruments.getRowJSON());
			first = false;
		}
		out.println("]}");

	} else {
		out.print("{\"status\":\"unauthorised\"}");
	}
%>
