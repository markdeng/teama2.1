import com.db.grad.teama21.UserBean;

import junit.framework.TestCase;

public class UserBeanTest extends TestCase {

	UserBean ub;
	
	public UserBeanTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		ub = new UserBean();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSetAndGetUid() {
		ub.setUid("Selv");
		assertEquals("Selv", ub.getUid());
	}

	public void testSetAndGetPwd() {
		ub.setPwd("Pass");
		assertEquals("Pass", ub.getPwd());

	}

	public void testSetAndCheckIsValid() {
		ub.setValid(true);
		assertTrue(ub.isValid());
	}
}
