/**
 * 
 */
package com.db.grad.teama21;

import junit.framework.TestCase;

/**
 * @author Graduate
 *
 */
public class DBFilterTest extends TestCase {

	DBFilter filter;
	
	/**
	 * @param name
	 */
	public DBFilterTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		filter = new DBFilter(DBFilter.Columns.ID, DBFilter.Comparators.Equals, "20321");
	}

	/**
	 * Test method for {@link com.db.grad.teama21.DBFilter#getFilterValue()}.
	 */
	public void testGetFilterValue() {
		assertEquals("20321", filter.getFilterValue());
	}

	/**
	 * Test method for {@link com.db.grad.teama21.DBFilter#getPreparedSQL()}.
	 */
	public void testGetPreparedSQL() {
		assertEquals("deal_id = ? ", filter.getPreparedSQL());
	}

}
