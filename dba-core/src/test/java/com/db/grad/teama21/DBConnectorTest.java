package com.db.grad.teama21;

import junit.framework.TestCase;

public class DBConnectorTest extends TestCase {
	DBConnector dbconn;
	
	public DBConnectorTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		dbconn = new DBConnector();
		
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testConnectToDatabase() {
	}
	
	public void testVerifyUserValid() {
		assertTrue(dbconn.verifyUser("selvyn", "gradprog2016"));
	}
	
	public void testVerifyUserInvalidPassword() {
		assertFalse(dbconn.verifyUser("selvyn", "invalid"));
	}
	
	public void testVerifyUserInvalidUid() {
		assertFalse(dbconn.verifyUser("selv", "gradprog2016"));
	}

}
