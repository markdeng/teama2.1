package com.db.grad.teama21;

public class DBFilter {
	public enum Comparators {
		Equals("="),
		GreaterThan(">"),
		GreaterEquals(">="),
		LessThan("<"),
		LessEquals("<="),
		NotEquals("<>"),
		Like("LIKE");
		
		private String sqlComparator;
		Comparators(String sqlComparator){
			this.sqlComparator = sqlComparator;
		}
		
		public String getSQLComparator() {
			return sqlComparator;
		}
	}
	
	public enum Columns {
		ID("deal_id"),
		Time("deal_time"),
		Type("deal_type"),
		Amount("deal_amount"),
		Quantity("deal_quantity"),
		Instrument("instrument_name"),
		Counterparty("counterparty_name");
		
		private String sqlColumnName;
		Columns(String sqlColumnName){
			this.sqlColumnName = sqlColumnName;
		}
		
		public String getSQLColumnName() {
			return sqlColumnName;
		}
	}
	
	Columns column;
	String filterValue;
	Comparators comparator;
	
	DBFilter(Columns column, Comparators comparator, String filterValue){
		this.column = column;
		this.comparator = comparator;
		this.filterValue = filterValue;
	}
	
	public String getFilterValue() {
		return filterValue;
	}
	
	public String getPreparedSQL() {
		return column.getSQLColumnName() + " " + comparator.getSQLComparator() + " ? ";
	}
	
}

