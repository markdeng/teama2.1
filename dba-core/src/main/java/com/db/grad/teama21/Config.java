package com.db.grad.teama21;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
	Properties prop = new Properties();

	public Config(InputStream in) {
		try {
			prop.load(in);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Config(File f) {
		try {
			prop.load(new FileInputStream(f));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Config() {
		try {
			prop.load(new FileInputStream("dbConnector.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Config f = new Config();
		f.printProps();
	}

	public void printProps() {
		prop.forEach((x, y) -> {
			System.out.println(x + "=" + y);
		});
	}

	public String getDBPath() {
		return prop.getProperty("dbPath");
	}

	public String getDBPwd() {
		return prop.getProperty("dbPwd");
	}

	public String getDBUser() {
		return prop.getProperty("dbUser");
	}
	
	public String getDBName() {
		return prop.getProperty("dbName");
	}
}
