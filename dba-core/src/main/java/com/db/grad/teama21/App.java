package com.db.grad.teama21;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        DBConnector db = new DBConnector();
        UserAccount ua = new UserAccount("samuel",	"gradprog2016@06");
        ua.validate();
        ArrayList<DBFilter> filters = new ArrayList<>();
        filters.add(new DBFilter(DBFilter.Columns.ID, DBFilter.Comparators.GreaterThan, "119879"));
        ResultSet s = db.getDeals(filters);
        try {
			while(s.next()) {
				System.out.println(s.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
