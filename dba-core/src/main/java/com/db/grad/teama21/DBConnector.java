package com.db.grad.teama21;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DBConnector {

	private static Connection conn = null;
	String dbHost, dbName, user, password;

	public DBConnector(String dbHost, String dbName, String user, String password) {
		this.dbHost = dbHost;
		this.dbName = dbName;
		this.user = user;
		this.password = password;
		if (conn == null) {
			try {
				conn = DriverManager.getConnection(dbHost + dbName,user,password);
			} catch (SQLException ex) {
				// handle any errors
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
			}
		}
	}
	
	public DBConnector() {
		ClassLoader classLoader = getClass().getClassLoader();
		File propertiesFile = new File(classLoader.getResource("dbConnector.properties").getFile());
		Config c = new Config(propertiesFile);
		this.dbHost = c.getDBPath();
		this.dbName = c.getDBName();
		this.user = c.getDBUser();
		this.password = c.getDBPwd();
		if (conn == null) {
			try {
				conn = DriverManager.getConnection(dbHost + dbName,user,password);
			} catch (SQLException ex) {
				// handle any errors
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
			}
		}
	}

	public void connect() {
		if (conn == null) {
			try {
				conn = DriverManager.getConnection(dbHost + "/" + dbName,user,password);

			} catch (SQLException ex) {
				// handle any errors
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
			}
		}
	}

	public boolean verifyUser(String uid, String pwd) {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM users WHERE user_id = ? AND user_pwd = ?;");
			ps.setString(1, uid);
			ps.setString(2, pwd);
			ResultSet r = ps.executeQuery();
			if (r.first()) {
				System.out.println(r.getString(1));
				System.out.println(r.getString(2));
				return true;
			}
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return false;
	}
	
	public ResultSet getDeals() {
		try {
			String statement = "SELECT deal_id, deal_time, deal_type, deal_amount, "
					+ "deal_quantity, instrument_name, counterparty_name, counterparty_status, counterparty_date_registered "
					+ "FROM deal d "
					+ "JOIN counterparty c ON d.deal_counterparty_id = c.counterparty_id "
					+ "JOIN instrument i ON d.deal_instrument_id = i.instrument_id;";
			PreparedStatement ps = conn.prepareStatement(statement);
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	
	public ResultSet getDeals(List<DBFilter> filters) {
		return getDeals(filters, DBFilter.Columns.ID, false);
	}

	public ResultSet getDeals(List<DBFilter> filters, boolean orderDesc) {
		return getDeals(filters, DBFilter.Columns.ID, orderDesc);
	}

	
	public ResultSet getDeals(List<DBFilter> filters, DBFilter.Columns orderColumn, boolean orderDesc) {
		if (filters.size() == 0) {
			return getDeals();
		} else try {
			String statement = "SELECT deal_id, deal_time, deal_type, deal_amount, "
					+ "deal_quantity, instrument_name, counterparty_name, counterparty_status, counterparty_date_registered "
					+ "FROM deal d "
					+ "JOIN counterparty c ON d.deal_counterparty_id = c.counterparty_id "
					+ "JOIN instrument i ON d.deal_instrument_id = i.instrument_id WHERE ";
			statement += filters.stream().map((x) -> x.getPreparedSQL()).collect(Collectors.joining(" AND "));
			statement += "ORDER BY " + orderColumn.getSQLColumnName() + (orderDesc ? " DESC" : " ASC");
			statement += ";";
			
			PreparedStatement ps = conn.prepareStatement(statement);
			
			int index = 1;
			for (DBFilter f: filters) {
				ps.setString(index++, f.filterValue);
			}
			
			System.out.println(statement);
			
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	public ResultSet getUsers() {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM users;");
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	public ResultSet getCounterparties() {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM counterparty;");
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	public ResultSet getInstruments() {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM instrument;");
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	public ResultSet getAverages() {
		try {
			String statement = "SELECT deal_type, i.instrument_name, AVG(deal_amount) " + 
					"FROM deal d " + 
					"LEFT JOIN instrument i ON d.deal_instrument_id = i.instrument_id " + 
					"GROUP BY deal_type, deal_instrument_id;";
			PreparedStatement ps = conn.prepareStatement(statement);
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	public ResultSet getFinalPositions() {
		try {
			String statement = "SELECT SUM(Movement) AS 'Net Trade', c.counterparty_name, c.counterparty_id, i.instrument_name, i.instrument_id " + 
					"FROM ( " + 
					"		SELECT SUM(deal_quantity)*-1 AS 'Movement', deal_type, deal_counterparty_id, deal_instrument_id " + 
					"		FROM deal " + 
					"		WHERE deal_type = 'S' " + 
					"		GROUP BY deal_type, deal_counterparty_id, deal_instrument_id " + 
					"		UNION " + 
					"		SELECT SUM(deal_quantity) AS 'Movement', deal_type, deal_counterparty_id, deal_instrument_id " + 
					"		FROM deal " + 
					"		WHERE deal_type = 'B' " + 
					"		GROUP BY deal_type, deal_counterparty_id, deal_instrument_id " + 
					"    ) AS table1 " + 
					"LEFT JOIN counterparty c ON c.counterparty_id = deal_counterparty_id " + 
					"LEFT JOIN instrument i ON i.instrument_id = deal_instrument_id " + 
					"GROUP BY deal_counterparty_id, deal_instrument_id;";
			PreparedStatement ps = conn.prepareStatement(statement);
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
	
	public ResultSet getRealised() {
		try {
			String statement = "SELECT SUM(Flow) AS 'Realised P/L', c.counterparty_name" + 
					"FROM (" + 
					"		SELECT SUM((deal_amount*deal_quantity))*-1 AS 'Flow', deal_counterparty_id" + 
					"		FROM deal" + 
					"		WHERE deal_type = 'B'" + 
					"		GROUP BY deal_counterparty_id" + 
					"		UNION" + 
					"		SELECT SUM((deal_amount*deal_quantity)) AS 'Flow', deal_counterparty_id" + 
					"		FROM deal" + 
					"		WHERE deal_type = 'S'" + 
					"		GROUP BY deal_counterparty_id" + 
					") AS table1" + 
					"LEFT JOIN counterparty c ON c.counterparty_id = deal_counterparty_id" + 
					"GROUP BY deal_counterparty_id;";
			PreparedStatement ps = conn.prepareStatement(statement);
			return ps.executeQuery();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return null;
	}
}

