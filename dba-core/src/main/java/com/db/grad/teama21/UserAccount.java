package com.db.grad.teama21;

public class UserAccount {
	private String uid;
	private String pwd;
    private static DBConnector db;

    public UserAccount() {};
    
    public UserAccount(String uid, String pwd) {
    	this.uid = uid;
    	this.pwd = pwd;
    }
	
	public boolean validate() {
		if (db==null) {
			db = new DBConnector();
		}
		return db.verifyUser(uid, pwd);
	}


	public String getUid() {
		return uid;
	}


	public void setUid(String uid) {
		this.uid = uid;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
